<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;

/**
 * Class logListener
 * @package App\EventListener
 */
class logListener
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * logListener constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // only act on some "User" entity
        if (!$entity instanceof User) {
            return;
        }

        $this->logger->info('modification on user id :' . $entity->getId() . ', at :' . $entity->getUpdatedate()->format('y-m-d h:m'));
    }
}
