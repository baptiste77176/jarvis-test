<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\User;
use App\Form\UserType;

/**
 * User controller.
 * @Route("/api", name="api_")
 */
class UserController extends FOSRestController
{
    /**
     * Lists all Users.
     * @Rest\Get("/listUser")
     *
     * @param UserRepository $userRepository
     * @return Response
     */
    public function getAllUser(UserRepository $userRepository): Response
    {
        $user = $userRepository->findall();
        return $this->handleView($this->view($user));
    }

    /**
     * Create User.
     * @Rest\Post("/createUser")
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function postUser(Request $request): Response
    {
        $user = new User();
        $date = new \DateTime();
        $form = $this->createForm(UserType::class, $user);

        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setCreationDate($date);
            $user->setUpdateDate($date);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }

        return $this->handleView($this->view($form->getErrors()));
    }

    /**
     * Find One user by id.
     *
     * @Rest\Get("/findUser/{id}")
     *
     * @param int $id
     * @param UserRepository $userRepository
     * @return Response
     */
    public function findUser(int $id, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['id' => $id]);
        return $this->handleView($this->view($user));
    }


    /**
     * Update One user by id.
     *
     * @Rest\Put("/updateUser/{id}")
     *
     * @param Request $request
     * @param int $id
     * @param UserRepository $userRepository
     * @return Response
     * @throws \Exception
     */
    public function updateUser(Request $request, int $id, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['id' => $id]);
        $date = new \DateTime();
        $form = $this->createForm(UserType::class, $user);

        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setUpdateDate($date);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view($form->getErrors()));
    }

    /**
     *  Removes the User resource.
     *
     * @Rest\Delete("/deleteUser/{id}")
     *
     * @param int $id
     * @param UserRepository $userRepository
     * @return Response
     */
    public function deleteUser(int $id, UserRepository $userRepository): Response
    {
        if (!empty($user = $userRepository->findOneBy(['id' => $id]))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }
        return $this->handleView($this->view(Response::HTTP_NO_CONTENT));
    }
}